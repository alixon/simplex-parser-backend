

module ParserHelpers

class UnrecognizedResponseError < RuntimeError ; end

  attr_reader :url
  @agent

  PAGES_LIMIT = 50
  PARSING_INTERVAL = 0.5

  def initialize()
    @agent = Mechanize.new do |agent|
      agent.user_agent_alias = 'Mac Safari'
    end
  end


  def build_url(uri = '')
    @url + uri
  end


  def prepare_listing(links)
    links.inject([]) do |memo, link|
    memo.push( 
      :id => URI::encode(link.attr('href')), 
      :text => link.text,
      :uri => link.attr('href')
    )
    end
  end


  def prepare_tables(tables)
    tables.inject([]) do |memo, table| 
      sections = table.search('tr').inject([]) do |memo, tr|
        memo.push( :name => tr.search('td:eq(1)').text, :content=> tr.search('td:eq(2)').text )
      end

      memo.push( :name => table.at('th').text, :content => sections )
    end
  end

end
