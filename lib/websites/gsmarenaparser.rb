

class GsmarenaParser
  include ParserHelpers

  def initialize
    @url = 'http://www.gsmarena.com/'
    super
  end


  def brands_action
    brands = []

    @agent.get( build_url ) do |page|
      links = page.search('//*[@id="body"]/aside/div[1]/ul/li/a')
      brands = prepare_listing(links)
    end

    { :type => :listing, :data => brands }
  end


  def models_action(brand_id)
    models = []
    uri = brand_id
    parsed_page_num = 1

    begin
      page = @agent.get( build_url(uri) )
      next_page = page.at('//div[@class="nav-pages"]/strong/following-sibling::a')
      links = page.search('#review-body ul li a')
      models.concat prepare_listing(links)

      parsed_page_num += 1
      sleep PARSING_INTERVAL

      break if next_page.nil?
      break if uri == next_page.attr('href')
      break if parsed_page_num >= PAGES_LIMIT
    end while uri = next_page.attr('href')

    { :type => :listing, :data => models }
  end


  def spec_action(model_id)
    page = @agent.get( build_url(model_id) )
    tables = page.search('#specs-list table')

    { :type => :table, :data => prepare_tables(tables) }
  end



  def search_action(query)
    data = []
    message = ''

    page = @agent.get(build_url).form_with(:id => 'topsearch') do |f|
      f.sName = query
    end.click_button

    if tables = page.search('#specs-list table') and tables.any?
      data = prepare_tables(tables)
      type = :table
    elsif listing = page.search('#review-body ul li a') and listing.any?
      data = prepare_listing(listing)
      type = :listing
    elsif page.body.include?('No phones found!')
      type = :error
      message = "We couldn't find any results :P"
    else
      raise UnrecognizedResponseError
    end

    { :type => type, :data => data, :message => message }
  end

end
