

class AutoyandexParser
  include ParserHelpers

  def initialize
    @url = 'https://auto.yandex.ru'
    super
  end


  def brands_action
    brands = []

    @agent.get(build_url) do |page|
        links = page.search('.list a.link')
        brands = prepare_listing(links)
    end

    { :type => :listing, :data => brands }
  end


  def models_action(brand_uri)
    models = []

    @agent.get(build_url(brand_uri)) do |page|
        links = page.search('.list a.link')
        models = prepare_listing(links)
    end

    { :type => :listing, :data => models }
  end


  def search_action(query)
    cars = []

    page = @agent.get(build_url).form_with(:action => '/offers') do |f|
      f.text = query
    end.click_button

    links = page.search('.b-table a.link')
    cars = prepare_listing(links)

    { :type => :listing, :data => cars }
  end

end
