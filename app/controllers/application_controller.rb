require './config/environment'

class ApplicationController < Sinatra::Base

  before do
    response.headers['Access-Control-Allow-Origin'] = '*'
  end

  configure do
    set :public_folder, 'public'
    set :views, 'app/views'
  end

  get "/" do
    File.read( File.join('public', "index.html") )
  end

  get "/:website/:action/*" do

    status = :success
    message = ''
    data = {}

    begin
      classname = "#{params[:website].capitalize}Parser"
      method = "#{params[:action]}_action"
      arguments = params[:splat].first.empty? ? [] : params[:splat].first.split('/')
      
      p = Object.const_get(classname).new
      data = p.send method, *arguments
    rescue Exception => e
      status = :error
      message = e.message
    end

    [
      status == :success ? 200 : 500,
      { :status => status, :message => message }.update(data).to_json
    ]
  end


end
