
require_relative "spec_helper"

describe AutoyandexParser do
  before(:all) { FakeWeb.allow_net_connect = false }
  before(:each) { @p = AutoyandexParser.new }

  describe 'instance' do
    describe 'when new' do
      subject { @p }
      it 'should contain website url' do
        expect( subject.build_url ).to include 'auto.yandex.ru'
      end
    end
  end


  describe 'brands_action' do
    subject { @p.brands_action }

    FakeWeb.register_uri(
      :get, 
      'https://auto.yandex.ru/', 
      :response => File.read('spec/autoyandex-home.html')
    )

    it{ is_expected.to be_instance_of Hash }
    it{ is_expected.to include :type => :listing }
    it{ is_expected.to have_key :data }
    it{ expect(subject[:data]).to be_instance_of Array }
    it{ expect(subject[:data].length).to eq 54 }
    it{ expect(subject[:data].last.keys).to include :id, :text, :uri }
  end



  describe 'models_action' do
    subject { @p.models_action('/offers/mercedes') }

    FakeWeb.register_uri(
      :get, 
      'https://auto.yandex.ru/offers/mercedes', 
      :response => File.read('spec/autoyandex-mercedes.html')
    )

    it{ is_expected.to be_instance_of Hash }
    it{ is_expected.to include :type => :listing }
    it{ is_expected.to have_key :data }
    it{ expect(subject[:data]).to be_instance_of Array }
    it{ expect(subject[:data].length).to eq 11 }
    it{ expect(subject[:data].last.keys).to include :id, :text, :uri }
  end



  describe 'search_action' do
    subject { @p.search_action('mercedes') }

    FakeWeb.register_uri(
      :get, 
      'https://auto.yandex.ru/offers?text=mercedes', 
      :response => File.read('spec/autoyandex-search.html')
    )

    it{ is_expected.to be_instance_of Hash }
    it{ is_expected.to have_key :data }
    it{ expect(subject[:data]).to be_instance_of Array }
    it{ expect(subject[:data].length).to eq 42 }
    it{ expect(subject[:data].last.keys).to include :id, :text, :uri }
  end

end
