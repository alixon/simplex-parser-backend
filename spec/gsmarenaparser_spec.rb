require_relative "spec_helper"

describe GsmarenaParser do
  before(:all) { FakeWeb.allow_net_connect = false }
  before(:each) { @p = GsmarenaParser.new }

  describe 'instance' do 
    describe 'when new' do
      subject { @p }
      it 'should contain website url' do 
        expect( subject.url ).to eq 'http://www.gsmarena.com/'
      end
    end
  end


  describe 'brands_action' do
    subject{ @p.brands_action }

    FakeWeb.register_uri(
      :get, 
      'http://www.gsmarena.com/', 
      :response => File.read('spec/brands.html')
    )

    it{ is_expected.to be_instance_of Hash }
    it{ is_expected.to include :type => :listing }
    it{ expect(subject[:data]).not_to be_empty }
    it{ expect(subject[:data].last).to be_instance_of Hash }
    it{ expect(subject[:data].last.keys).to include(:id, :text) }
    it{ expect(subject[:data].length).to eq 36 }

  end


  describe 'models_action' do
    describe 'phone models list without pagination' do
      subject{ @p.models_action('phone-models-without-pagination.html') }

      FakeWeb.register_uri(
        :get, 
        'http://www.gsmarena.com/phone-models-without-pagination.html', 
        :response => File.read('spec/phone-models-without-pagination.html')
      )

      it{ is_expected.to be_instance_of Hash }
      it{ expect(subject[:type]).to eq :listing }
      it{ expect(subject[:data]).not_to be_empty }

      it "should call 'prepare_listing' only once" do
        expect(@p).to receive(:prepare_listing).once.and_call_original
        subject
      end
    end

    describe 'phone models list with pagination' do
      subject{ @p.models_action('pantech-phones-f-32-0-p1.php') }

      (1..3).each do |page_index|
        FakeWeb.register_uri(
          :get, 
          "http://www.gsmarena.com/pantech-phones-f-32-0-p#{page_index}.php", 
          :response => File.read("spec/phone-models-page-#{page_index}.html")
        )
      end

      it{ is_expected.to be_instance_of Hash }
      it{ expect(subject[:data].length).to eq 72 }

      it "should call 'prepare_listing' exactly 3 times" do
        expect(@p).to receive(:prepare_listing).exactly(3).times.and_call_original
        subject
      end
    end

  end


  describe 'spec_action' do
    subject{ @p.spec_action('phone-spec.html') }

    FakeWeb.register_uri(
      :get, 
      'http://www.gsmarena.com/phone-spec.html', 
      :response => File.read('spec/phone-spec.html')
    )

    it{ is_expected.to be_instance_of Hash }
    it{ expect(subject[:type]).to eq :table }

    it{ expect(subject[:data]).to be_instance_of(Array) }
    it{ expect(subject[:data].last).to include :name => 'Misc' }

    it{ expect( subject[:data].last[:content] ).to be_instance_of Array }
    it{ expect( subject[:data].last[:content].last ).to include :name => 'Price group' }

  end



  describe 'search_action' do
    describe 'search result is phone spec' do
      subject{ @p.search_action('motorola e390') }

      FakeWeb.register_uri(
        :get, 
        'http://www.gsmarena.com/results.php3?sQuickSearch=yes&sName=motorola+e390', 
        :response => File.read('spec/phone-spec.html')
      )

      it{ is_expected.to be_instance_of Hash }
      it{ expect(subject[:type]).to eq :table }
      it{ expect(subject[:data]).to be_instance_of Array }
    end

    describe 'search result is a list of phone models' do
      subject{ @p.search_action('apple') }

      FakeWeb.register_uri(
        :get, 
        'http://www.gsmarena.com/results.php3?sQuickSearch=yes&sName=apple', 
        :response => File.read('spec/search-resut-listing.html')
      )

      it{ is_expected.to be_instance_of Hash }
      it{ expect(subject[:type]).to eq :listing }
      it{ expect(subject[:data]).to be_instance_of Array }
    end

    describe 'search result page is empty' do
      subject{ @p.search_action('extraterrestrialphone') }

      FakeWeb.register_uri(
        :get, 
        'http://www.gsmarena.com/results.php3?sQuickSearch=yes&sName=extraterrestrialphone', 
        :response => File.read('spec/search-resut-empty.html')
      )

      it{ is_expected.to be_instance_of Hash }
      it{ expect(subject[:type]).to eq :error }
      it{ expect(subject[:data]).to be_instance_of Array }
      it{ expect(subject[:message]).to be_instance_of String }
    end
  end

end
