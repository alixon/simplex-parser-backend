require_relative "spec_helper"

def app
  ApplicationController
end

describe ApplicationController do
  before(:all) { FakeWeb.allow_net_connect = false }

  describe 'app should responds' do
    it "responds with HTTP 200 status" do
      get '/'
      expect(last_response.status).to eq(200)
      expect(last_response.body).to include("loading")
    end
  end


  describe 'responds to brands requst' do
    FakeWeb.register_uri(
      :get, 
      'http://www.gsmarena.com/', 
      :response => File.read('spec/brands.html')
    )

    it "responds with HTTP 200 status" do
      get '/'
      expect(last_response.status).to eq(200)
    end

    it "respond" do
      get '/gsmarena/brands/'
      expect(last_response.status).to eq(200)
      expect(last_response.body).to include("success")
      expect(last_response.body).to include("text")
      expect(last_response.body).to include("data")
      expect(last_response.body).to include("uri")
      expect(last_response.body).to include("id")
    end

  end

  describe 'responds to phone models request' do
    FakeWeb.register_uri(
      :get, 
      'http://www.gsmarena.com/plum-phones-72.php', 
      :response => File.read('spec/phone-models-without-pagination.html')
    )

    it "should contain models information" do
      get '/gsmarena/models/plum-phones-72.php'
      expect(last_response.status).to eq(200)
      expect(last_response.body).to include("success")
      expect(last_response.body).to include("text")
      expect(last_response.body).to include("data")
      expect(last_response.body).to include("uri")
      expect(last_response.body).to include("id")
    end

    it "should fall on wrand requst" do
      get '/gsmarena/models/'
      expect(last_response.status).to eq(500)
      expect(last_response.body).to include("wrong number of arguments")
      expect(last_response.body).to include("error")
    end
  end


  describe 'responds to phone specification' do
    FakeWeb.register_uri(
        :get, 
        'http://www.gsmarena.com/plum_hammer-5199.php', 
        :response => File.read('spec/phone-spec.html')
    )

    it "should contain spec info" do
      get '/gsmarena/spec/plum_hammer-5199.php'
      expect(last_response.status).to eq(200)
      expect(last_response.body).to include("success")
      expect(last_response.body).to include("data")
      expect(last_response.body).to include("Network")
      expect(last_response.body).to include("Misc")
    end
  end


  describe 'responds to search query' do
    FakeWeb.register_uri(
      :get, 
      'http://www.gsmarena.com/results.php3?sQuickSearch=yes&sName=motorola', 
      :response => File.read('spec/search-resut-listing.html')
    )

    it "should contain" do
      get '/gsmarena/search/motorola'
      expect(last_response.status).to eq(200)
      expect(last_response.body).to include("success")
      expect(last_response.body).to include("text")
      expect(last_response.body).to include("data")
      expect(last_response.body).to include("listing")
    end
  end
end
